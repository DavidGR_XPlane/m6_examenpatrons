package Main;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class testDB {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		ResultSet rs2 = null;
		try {
			// Utilitzem el nostre objecte connection per a fer la conexió amb la nostra
			// base de dades.
			// l'String que hem de passar com a argument es del tipus:
			// "jdbc:mysql://localhost/[nomSchema]?user=[user]&password=[password]"
			conn = DriverManager.getConnection("jdbc:mysql://localhost/powerrangers?" + "user=root&password=super3");

			// L'enunciat que li llençarem a la nostra BBDD (Query)
			stmt = conn.createStatement();
			// Si les query sempre seran SELECTS, podem utilitzar el métode executeQuery
			rs = stmt.executeQuery("SELECT * FROM herois");

			// Si no, fem servir el métodes execute, que si la query insertada ens retorna
			// un ResultSet ens tornarà true i si no, false.
			Statement stmt2 = conn.createStatement();
			// Si retorna true -> podem iterar el ResultSet
			if (stmt2.execute("SELECT * FROM herois WHERE nom = 'Morcilla'"))
				;
			{
				rs2 = stmt2.getResultSet();
				while (rs2.next()) {
					System.out.println(rs2.getString("nom"));
				}
			}

			while (rs.next()) {
				// Display values
				System.out.println("ID: " + rs.getInt("idPowerRanger"));
				System.out.println("Name: " + rs.getString("nom"));
				System.out.println("Color: " + rs.getString("color"));
				System.out.println("Arma:" + rs.getString("arma"));
				System.out.println("Epoca: " + rs.getInt("epoca"));
			}

		} catch (SQLException ex) {
			// handle any errors
			System.out.println("SQLException: " + ex.getMessage());
			System.out.println("SQLState: " + ex.getSQLState());
			System.out.println("VendorError: " + ex.getErrorCode());
			// SEMPRE TANQUEM LES CONEXIONS SIUSPLAA AA A A AU
		} finally {

			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException sqlEx) {
				} // ignore
				rs = null;
			}
			if (stmt != null) {

				try {

					stmt.close();
				} catch (SQLException sqlEx) {
				} // ignore
				stmt = null;
			}
		}
	}

}
